You can view these docs on a local machine. First you will need to install mkdocs:

Install mkdocs:
```
sudo python3 -m pip install mkdocs
```

Then, once mkdocs is installed, you should build the docs:

Build docs:
```
mkdocs serve
```

Once the docs are built, then you can view them locally at http://127.0.0.1:8000/.
