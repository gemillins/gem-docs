# Welcome to the GEM Docs
This is the documentation page for the GEM vehicle project.

## Writing documentation
All the documentation is written as markdown files.
Here is a short tutorial on writing documentation.

#### 1. Creating a new file
First, you will need to create a new file.
This file should be stored in the appropriate folder in `\docs`, and should be named `filename.md`.

#### 2. Write documentation using markdown
Write the file using markdown.
A cheatsheat for markdown can be found [here](https://www.markdownguide.org/cheat-sheet/).
Note that titles using `# Title` or `## Subtitle` will appear in the sidebar.

#### 3. Add page to the menu
Once you create a new file and want it to appear on the nav bar, you should add it to `mkdocs.yml`.
Please see the example below on how to add your file to the nav bar.

```
nav:
    - Home: 'index.md'
    - Simulator:
        - 'Controller interface': 'test.md'
        - Perception: 'perception.md'
        - Vehicles: 'vehicles.md'
    - 'Scenario creation':
        - 'Scenario runner': 'runner.md'
        - 'Map creation': 'map.md'
    - 'Testing Pipeline': 'testing.md'
    - Website:
        - 'Site maintenance': 'website.md'
        - Submissions: 'submissions.md'
    - Distribution:
        - 'AWS image': 'license.md'
        - 'Docker image': 'docker.md'
```

## What should be included in the documentation?
We want GEM to be widely used and hopefully run for a long time.
You may be writing a tutorial for future students who may not have been around for the creation of GEM.
Thus, you should write detailed documentation.
Below is a list of some information that may be relevant to document.

- Requirements for running
- Step by step instructions for running
- Expected output
- Potential issues
- Anything else that may be important
